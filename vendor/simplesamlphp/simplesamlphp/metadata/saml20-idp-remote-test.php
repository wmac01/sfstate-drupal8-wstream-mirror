<?php
/**
 * SAML 2.0 remote IdP metadata for simpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://rnd.feide.no/content/idp-remote-metadata-reference
 */

 $metadata['https://idp-test.sfsu.edu/idp/shibboleth'] = array (
   'entityid' => 'https://idp-test.sfsu.edu/idp/shibboleth',
   'contacts' =>
   array (
   ),
   'metadata-set' => 'saml20-idp-remote',
   'SingleSignOnService' =>
   array (
     0 =>
     array (
       'Binding' => 'urn:mace:shibboleth:1.0:profiles:AuthnRequest',
       'Location' => 'https://idp-test.sfsu.edu/idp/profile/Shibboleth/SSO',
     ),
     1 =>
     array (
       'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
       'Location' => 'https://idp-test.sfsu.edu/idp/profile/SAML2/POST/SSO',
     ),
     2 =>
     array (
       'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign',
       'Location' => 'https://idp-test.sfsu.edu/idp/profile/SAML2/POST-SimpleSign/SSO',
     ),
     3 =>
     array (
       'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
       'Location' => 'https://idp-test.sfsu.edu/idp/profile/SAML2/Redirect/SSO',
     ),
   ),
   'SingleLogoutService' =>
   array (
     0 =>
     array (
       'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
       'Location' => 'https://idp-test.sfsu.edu/idp/profile/SAML2/Redirect/SLO',
       'ResponseLocation' => 'https://idp-test.sfsu.edu/idp/profile/SAML2/Redirect/SLO',
     ),
     1 =>
     array (
       'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
       'Location' => 'https://idp-test.sfsu.edu/idp/profile/SAML2/POST/SLO',
       'ResponseLocation' => 'https://idp-test.sfsu.edu/idp/profile/SAML2/POST/SLO',
     ),
     2 =>
     array (
       'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
       'Location' => 'https://idp-test.sfsu.edu/idp/profile/SAML2/SOAP/SLO',
     ),
   ),
   'ArtifactResolutionService' =>
   array (
     0 =>
     array (
       'Binding' => 'urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding',
       'Location' => 'https://idp-test.sfsu.edu:8443/idp/profile/SAML1/SOAP/ArtifactResolution',
       'index' => 1,
     ),
     1 =>
     array (
       'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
       'Location' => 'https://idp-test.sfsu.edu:8443/idp/profile/SAML2/SOAP/ArtifactResolution',
       'index' => 2,
     ),
   ),
   'keys' =>
   array (
     0 =>
     array (
       'encryption' => true,
       'signing' => true,
       'type' => 'X509Certificate',
       'X509Certificate' => '
 MIIDMDCCAhigAwIBAgIVAOOHOpN0I4d4eKeF1jUAG6CmYvo4MA0GCSqGSIb3DQEB
 BQUAMBwxGjAYBgNVBAMTEWlkcC10ZXN0LnNmc3UuZWR1MB4XDTEwMDExMjIwNTQ0
 MFoXDTMwMDExMjIwNTQ0MFowHDEaMBgGA1UEAxMRaWRwLXRlc3Quc2ZzdS5lZHUw
 ggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCbpiaUj9O+1k47VdBHdwpB
 GYUt2YN07705q+7ILi99U/jqhTVTJGlGNrK7Xrd9cLscvMey24lI92Tdv8XxOHJ5
 GZE2z2UTaIVyunkHqHxxFwXIV1gbA3wS9X8zsEjPS7GtYqbXXLNBsI0hqqvuT2Jz
 jLZ1aNf0sLH9piWHzKXamBS43nAxPL03q7fi8thPMeUe2Jm3IFNTQ3ky/cfMTItW
 x7eTKTxZF55WHtkk9cwFB8nsWUvw2sZP2b7LCn0bA2hrwEQs4MedT9AgosD2xnoB
 3kKJDyAhwByMfdLVSEOLs+7ur3m8Li4r6D5tahW8n0V4wXE7lAwpyhlRlSjtgvuL
 AgMBAAGjaTBnMEYGA1UdEQQ/MD2CEWlkcC10ZXN0LnNmc3UuZWR1hihodHRwczov
 L2lkcC10ZXN0LnNmc3UuZWR1L2lkcC9zaGliYm9sZXRoMB0GA1UdDgQWBBTgcgbU
 IV+4Fo+R+frwNxYtzriQlDANBgkqhkiG9w0BAQUFAAOCAQEAQB1zPcyyPsspbil+
 8m8rH69kuUPpEfdnHKlvEWpIUYZhXZscugD3Ji5EKIj7VD/x/YotBo5p1DB+A++K
 YOB+JIKqZqrIZTvTE2CPTRS1NvQl33qxPREMLmMVxaxovxAsZbsUbQ6K61yrBBcj
 8XQ/GU4ZhWnJVdop2/uP1xTxjo8HdkcU7yIlHgbUp/Cd7wBzTFoqxBBpDfsDgeWx
 v8wigW3bN2/Ea7n9tjMMAf13Wx9OhCJCy9bz65Mr6dfauzN2khCwnd1BuiDuyuoi
 xqLGi49iGRk7ejv/IkvriuvSCZTBdhFxTD5b24FXHLcaSTX4DKRtY6FRmwVy3vaE
 /n3gMw==
                     ',
     ),
   ),
   'scope' =>
   array (
     0 => 'sfsu.edu',
   ),
 );
