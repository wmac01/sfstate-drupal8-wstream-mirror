<?php

/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */


/**
 * Implements HOOK_install_tasks_alter()
 *
 * Allows to alter the install tasks
 */
function sfstatedrupal_install_tasks_alter(&$tasks, $install_state) {
  $tasks['install_configure_form']['display'] = FALSE;
  $tasks['install_configure_form']['run'] = INSTALL_TASK_SKIP;
}

